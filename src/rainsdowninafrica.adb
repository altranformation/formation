with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Exceptions;
package body RainsDownInAfrica is

   function Get_Blessing(days : positive) return positive
   with pre => days > 1 and days < 50 -- pre-condition
   is
   begin
      return days * 37 + 200;
   end Get_Blessing;


   days : positive;
   excp : Exception;
   procedure makeRain is
   begin
      Ada.Text_IO.Put_Line("Africa : days before rains, mult of 7 only");
      Get(days);

      if days mod 7 /= 0 then

         Ada.Exceptions.Raise_Exception(excp'Identity, " *Pepe Sadge* No Rains, it is not wednesday my dudes!");

         raise excp with " *WidePeepo Sadge* No Rains ";

      end if;

      Put_Line(days'Image & " days " & ", Total Blessing : " & Get_Blessing(days)'Image);

   exception

      when e : excp => Put_Line("*WidePeepo Happy* : " & Ada.Exceptions.Exception_Message(e));
      when e : CONSTRAINT_ERROR  => Put_Line("*WidePeepo Happy* CONSTRAINT_ERROR: " & Ada.Exceptions.Exception_Message(e));
      --when e : others  => Put_Line("*Pepe Laugh* others error: " & Ada.Exceptions.Exception_Message(e));

   end makeRain;

end RainsDownInAfrica;
