with Ada.Text_IO; use Ada.Text_IO;
package body EuropaUniversalis is

   procedure CastilleAragon is

      type positionGPS is record
         lg : Float;
         la : Float;
         isLand : Boolean;
         nearCity : String (1 .. 10);
      end record;

      elem : positionGPS := (-1.0, 37.2, TRUE, "Valencia  ");

   begin
      Ada.Text_IO.Put_Line("Event : Iberian marriage at : " & elem.nearCity & elem.lg'Image & elem.la'Image);
   end CastilleAragon;

   procedure SPQR is

      package RegionToPacify is
         type Instance is tagged record
            name : String (1 .. 10);
            region : String (1 .. 2);
            days : positive;
         end record;
         function init
           (
            name : String := "Egypte    ";
            region : String := "Ef";
            days : positive := 1
           )return Instance;
         procedure display(self : in out Instance);
         procedure takeMoreTime(self : in out Instance; days : integer);
         procedure forceMarche(self : in out Instance; days : integer);
         function Hours(self : Instance)return positive;
         procedure displayWithStars(self :in out Instance'class);
      end RegionToPacify;

      package body RegionToPacify is
         function init
           (
            name : String  := "Egypte    ";
            region : String := "Ef";
            days : positive := 1
           )return Instance is
            i : Instance := (name , region , days);
         begin
            return i;
         end Init;
         procedure display(self :in out Instance) is
         begin
            Ada.Text_IO.Put_Line("Pax Romana : " & self.name & self.region & self.days'Image);
         end display;
         procedure takeMoreTime(self :in out Instance; days : integer) is
         begin
            self.days := self.days + days;
         end takeMoreTime;
         procedure forceMarche(self : in out Instance; days : integer) is
         begin
            self.takeMoreTime(-days);
         end forceMarche;
         function Hours(self : Instance)return positive is
         begin
            return 24 * self.days;
         end;
         procedure displayWithStars(self :in out Instance'class) is -- virtual
         begin
            Ada.Text_IO.Put_Line("** - **");
            self.display;
            Ada.Text_IO.Put_Line("** - **");
         end displayWithStars;
      end RegionToPacify;

      package RegionToPacifyIsland is
         type Instance is new RegionToPacify.Instance with record
            islandeName : String (1 .. 3);
         end record;
         overriding function init
           (
            name : String := "Egypte    ";
            region : String := "Ef";
            days : positive := 1
           )return Instance;
         function init
           (
            name : String := "Egypte    ";
            region : String := "Ef";
            days : positive := 1;
            islandeName : String := "VEN"
           )return Instance;
          overriding procedure display(self :in out Instance);
      end RegionToPacifyIsland;

      package body RegionToPacifyIsland is
         overriding function init
           (
            name : String  := "Egypte    ";
            region : String := "Ef";
            days : positive := 1
           )return Instance is
            i : Instance := (name , region , days, "VEN");
         begin
            return i;
         end Init;
          function init
           (
            name : String  := "Egypte    ";
            region : String := "Ef";
            days : positive := 1;
            islandeName : String := "VEN"
           )return Instance is
            i : Instance := (name , region , days, islandeName);
         begin
            return i;
         end Init;
         overriding procedure display(self :in out Instance) is
         begin
            RegionToPacify.Instance(self).display;
            Ada.Text_IO.Put_Line(" - " & self.islandeName);
         end display;
      end RegionToPacifyIsland;


      destination : RegionToPacify.Instance := RegionToPacify.Init("Egypte    ", "Eg", 10);
      destinationI : RegionToPacifyIsland.Instance := RegionToPacifyIsland.Init("Venise    ", "Ve", 10, "Ven");

   begin
      --RegionToPacify.display(destination);

      destination.takeMoreTime(2);
      destination.displayWithStars;
      destination.forceMarche(2);
      destination.display;
      Ada.Text_IO.Put_Line("Pax Romana : " & destination.Hours'Image);
      destinationI.displayWithStars;
   end SPQR;



end EuropaUniversalis;
