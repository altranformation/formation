with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Exceptions;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO.Unbounded_IO;
with Ada.Calendar; use Ada.Calendar;
with Ada.Calendar.Formatting;
with Ada.Containers.Vectors;
with Ada.Directories; Use Ada.Directories;

package body WelcomeToTheJungle is

   type tinfo is record
      dest : Unbounded_String;
      name : String (1 .. 10);
      tel : String (1 .. 10);
   end record;

   procedure FortunateSon is

      info : tinfo;
      destL, nameL, telL : natural;
      currentTime : Ada.Calendar.Time := Ada.Calendar.Clock;
      nextTime : Ada.Calendar.Time := Ada.Calendar.Clock;
      delayTime : duration;

      txtfile : File_Type;
   begin

      Put_Line("WelcomeToTheJungle");
      Put_Line("Dest ?");
      Ada.Text_IO.Unbounded_IO.Get_Line(info.dest);
      Put_Line("Name ?");
      Get_Line(info.name, nameL);
      Put_Line("Tel ?");
      Get_Line(info.tel, telL);

      Put_Line("Go to " & To_String(info.dest) & " : " & info.name(1..nameL) &" loaded reduced space");

      --for i in 1 .. Ada.Strings.Unbounded.Length(info.dest) loop
        -- if i = info.destend and then Ada.Strings.Unbounded.Element(info.dest, i) = ' ' then
          --  Ada.Strings.Unbounded.Delete(info.dest, i, i);
         --end if;
        --end loop;

      info.dest := Ada.Strings.Unbounded.Trim(info.dest, Ada.Strings.both);

      Put_Line("Go to " & To_String(info.dest) & " : " & info.name(1..nameL) &" loaded");

      Put_Line("Year : " & Ada.Calendar.Year(currentTime)'Image);
      -- currentTime +- d => time
      -- currentTime - currentTime => duration + impossible
       Put_Line("CurrentTime : " & Ada.Calendar.Formatting.Image(currentTime));
   --exception
   -- 4w3d
      delayTime := duration(86400 * ((7 * 4) + 3) );
      nextTime := currentTime + delayTime;

      Put_Line("NextTime : " & Ada.Calendar.Formatting.Image(nextTime));

      if Exists("Info.csv") then
        Ada.Text_IO.Open(txtfile, Append_File, "Info.csv");
      else
        Ada.Text_IO.Create(txtfile, Append_File, "Info.csv");
      end if;

      Ada.Text_IO.Put_Line(txtfile, To_String(info.dest) & " , " & info.name(1..nameL) & " , " & info.tel(1..telL));
      Ada.Text_IO.Close(txtfile);


   end FortunateSon;


   procedure RiceField is

      type tAge is range 0 .. 120;

      min : tAge := 120;
      max : tAge := 0;

      package TV is new Ada.Containers.Vectors
      (Index_Type=>Natural,
       Element_Type=>tAge);
      vectAge : TV.Vector;

      input : tAge;
   begin

      Put_Line("Welcome to the Rice Field");

      Put_Line("Age ?");

      loop

         Get(natural(input));
         exit when input = 0;
         vectAge.append(input);

         --if input < min then
           --min := input;
         --end if;
         --if input > max then
           --max := input;
         --end if;

      end loop;

      for i in 0 .. natural(vectAge.length)-1 loop
         if vectAge(i) < min then
           min := vectAge(i);
         end if;
         if vectAge(i) > max then
           max := vectAge(i);
         end if;
      end loop;


     Put_Line("Age min : " & min'Image & " and  age max : " & max'Image);

   end RiceField;

end WelcomeToTheJungle;
