package America is

   type tprice is digits 6 range 0.0 .. 10000.0;
   procedure OCanada(plane : tprice := 1000.0; pricePerDay : tprice := 50.0);

private

   type tperiode is range 0 .. 100;

end America;
