with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
package body America is

   function GETperiode return tperiode is
      periodG : integer;
      period : tperiode;
   begin
      Put_Line("Insert Periode");
      Get(periodG);
      period := tperiode(periodG);
      return period;
   end GETperiode;

   procedure OCanada(plane : tprice := 1000.0; pricePerDay : tprice := 50.0) is

      type tdiscount is digits 3 range 0.0 .. 100.0;
      discount : tdiscount := 20.0;
      total : tprice;

      period : tperiode := 7;

      type tdepartureDay is (M,Tu,W,Th,F,Sa,Su);
      type tweek is NEW tdepartureDay range M .. F;
      subtype tweek2 is tdepartureDay range Tu .. Th;
      departureDay : tdepartureDay := Sa;
      c1 : character := 'A';
      chaine : string := "Go to Alderaan with a -" & discount'Image & "% on " & plane'Image & "e on plane and " & period'Image & "j at " &
        pricePerDay'Image & "e/d";
      --infoTotal : string (1 .. 50);
      com1Day : tdepartureDay := Tu;
      com2Day : tweek2 := W;
      comp : boolean := com1Day /= com2Day;

      type tKm is range  0 .. 6500;
      KmPlane : tKm := 6500;
      KmWalk : tKm := 22;
      type tArrayKm is Array(tperiode) of tKm;
      arrayKm : tArrayKm;


      --type tarrayDyn is array(tperiode range<>) of tKm;
      --arrayDyn : tarrayDyn (tperiode) ;
      begin

      Put_Line("Welcome to VoyageVoyageDesireLess");
      period := GETperiode;
      Put_Line(chaine);
      total := plane + tprice(float(pricePerDay) * float(period));
      total := total * tprice(1.0 - float(discount /100.0)) ;
      --infoTotal := "For only " & total'Image & "e";
      Put_Line("For only " & total'Image & "e");
      Put_Line("Departure Day is " & departureDay'Image);
      Put_Line("Is it Su ? " & boolean(departureDay=Su)'Image);

      case period is
      when 0 .. 6
         => Ada.Text_IO.Put_Line("With Millenium Falcon in less than 12 parsec");
      when 7 | 14 | 21 | 28
         => Ada.Text_IO.Put_Line("In the company of Ruby Rhod : Super Green");
      when others
         => Ada.Text_IO.Put_Line("Trough the Star Gate");
      end case;

      for i in 0 .. period loop
         if i = 0 or i = period then
            Ada.Text_IO.Put_Line("Day " & i'image & ": Plane");
            arrayKm(i) := KmPlane;
         elsif boolean( (i mod 4) = 0) = TRUE then
            Ada.Text_IO.Put_Line("Day " & i'image & ": Rest");
            arrayKm(i) := 0;
         else
            Ada.Text_IO.Put_Line("Day " & i'image & ": Walk");
            arrayKm(i) := KmWalk;
         end if;
      end loop;

      declare
         sommeKm : integer := 0;
         begin
         for i in 0 .. period loop
            sommeKm := sommeKm + integer(arrayKm(i));
         end loop;
         Ada.Text_IO.Put_Line("Sum of km " & sommeKm'image);
      end;

   end OCanada;
end America;






