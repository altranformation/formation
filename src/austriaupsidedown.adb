with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Numerics.Discrete_Random;
with Ada.Containers.Vectors;
package body AustriaUpsideDown is


   procedure everythingHereWillKillYou is
      type tchance is range 1..20;
      package chanceWar is new Ada.Numerics.Discrete_Random(tchance);
      chance : chanceWar.generator;

      package TV is new Ada.Containers.Vectors
      (Index_Type=>Natural,
       Element_Type=>tchance);
      vectRandom : TV.Vector;

      warChance : tchance;

      function GetRandomChance(vect : in out TV.Vector)return tchance is
         begin
         warChance := chanceWar.random(chance);
            if not vectRandom.contains(warChance) then
               vectRandom.append(warChance);
               Put_Line("Chance : " & warChance'Image);
         end if;
         return warChance;
      end GetRandomChance;

       task display is
         entry startT;
      end display;


      task emuWar;
      task body emuWar is

      begin
         Put_Line("*WidePeepo Happy* Start war against emu");

         while integer(vectRandom.length) < 4 loop
            delay 1.0;
            warChance := GetRandomChance(vectRandom);

         end loop;
         --delay 2.0;

        -- if chanceWar.random(chance) < 2 then
         --   Put_Line("*WidePeepo Happy* Win war to emu");
         --else
         --   Put_Line("*WidePeepo Sadge* Lose war to emu");
         --end if;

         Put_Line("*WidePeepo sadge* End war against emu");

         -- sync
         display.startT;
      end emuWar;



      task body display is

      begin
        accept startT;
        for i in 0..3 loop
            Put_Line("SYNC : " & tchance'Image(vectRandom(0)));
        end loop;
          -- synchro end task
        Put_Line("Bye ");
      end display;

   begin

      Put_Line("Welcome to kangooroo land");
      chanceWar.reset(chance);
      --emuWar.Start;




  -- exception

   end everythingHereWillKillYou;

end AustriaUpsideDown;
